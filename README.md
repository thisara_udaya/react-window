##About this project
This is a example application created with create-react-app to demonstrate application of react-window (https://github.com/bvaughn/react-window).

#How to setup

##pre-requisites

- npm
- react > 16.8

##steps

1. Clone the project
2. npm install

#How to run

1. npm start
2. http://localhost:3000
