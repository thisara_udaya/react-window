import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import FixedSizeList from './components/listFixedSized';
import VariableSizeList from './components/listVariableSized';
import AutoSizerList from './components/listAutoSized';
import FixedSizeGrid from './components/gridFixedSized';
import VariableSizeGrid from './components/gridVariableSized';
import AutoSizeGrid from './components/gridAutoSized';
import ListInfiniteLoader from './components/listInfiniteLoader';

function App() {
  return (
    <Router>
    <Container fluid>
      <Row>
        <Col>
          <div className='headStyle'>Demo - react-window</div>
        </Col>
      </Row>
      <Row className='contentArea'>
        <Col lg={2} className='navLinks'>
          <div class="p-1 mb-2 bg-light text-white">
            <Link class="text-info" to="/fixed-size-list">Fixed List</Link>
          </div>
          <div class="p-1 mb-2 bg-light text-white">
            <Link class="text-info" to="/variable-size-list">Variable List</Link>
          </div>
          <div class="p-1 mb-2 bg-light text-white">
            <Link class="text-info" to="/fixed-size-grid">Fixed Grid</Link>
          </div>
          <div class="p-1 mb-2 bg-lightv text-white">
            <Link class="text-info" to="/variable-size-grid">Variable Grid</Link>
          </div>
          <div class="p-1 mb-2 bg-light text-white">
            <Link class="text-info" to="/auto-size-list">Auto List</Link>
          </div>
          <div class="p-1 mb-2 bg-light text-white">
            <Link class="text-info" to="/auto-size-grid">Auto Grid</Link>
          </div>
          <div class="p-1 mb-2 bg-light text-white">
            <Link class="text-info" to="/infinite-list">Infinite List</Link>
          </div>
        </Col>
        <Col lg={10}>
          <Switch>
            <Route path ='/fixed-size-list' component={FixedSizeList}/>
            <Route path ='/variable-size-list' component={VariableSizeList}/>
            <Route path ='/auto-size-list' component={AutoSizerList}/>
            <Route path ='/fixed-size-grid' component={FixedSizeGrid}/>
            <Route path ='/variable-size-grid' component={VariableSizeGrid}/>
            <Route path ='/auto-size-grid' component={AutoSizeGrid}/>
            <Route path = '/infinite-list' component={ListInfiniteLoader}/>
          </Switch>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className='footerStyle'>@ThisaraAlawala | mytechblogs.com</div>
        </Col>
      </Row>
    </Container>
    </Router>
  );
}

export default App;
