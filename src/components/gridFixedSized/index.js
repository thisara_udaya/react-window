import React, { Component } from 'react';
import { FixedSizeGrid as Grid } from 'react-window';

import CellTemplate from '../commons/rowTemplates/gridCellTemplate';
import FormHeader from '../commons/formHeaders/gridFormHeader';

import { ALIGN_AUTO } from '../commons/constants/formHeaderConstants';

class FixedSizeGrid extends Component{

    gridRef = React.createRef();

    state = {
        scrollToRowNumber : 0,
        scrollToColumnNumber : 0,
        align : ALIGN_AUTO
    }

    onScrollTo = (e) => {
        this.gridRef.current.scrollToItem({
            columnIndex: this.state.scrollToColumnNumber,
            rowIndex: this.state.scrollToRowNumber,
            align: this.state.align
        });
    }

    onUpdateScrollToRow = (e) => {
        if(e){
            this.setState({
                scrollToRowNumber : e.target.value
            })
        }
    }

    onUpdateScrollToColumn = (e) => {
        if(e){
            this.setState({
                scrollToColumnNumber : e.target.value
            })
        }
    }

    onUpdateAlign = (e) => {
        if(e){
            this.setState({
                align : e.target.value
            })
        }
    }

    render(){
        return (
            <>
            <FormHeader formHeader={'Fixed Size Grid'} onUpdateScrollToRow={this.onUpdateScrollToRow} onUpdateScrollToColumn={this.onUpdateScrollToColumn} onScrollTo={this.onScrollTo} onUpdateAlign={this.onUpdateAlign}/>
            <Grid
                columnCount={1000}
                columnWidth={100}
                height={500}
                rowCount={1000}
                rowHeight={35}
                width={900}
                ref={this.gridRef}
            >
                {CellTemplate}
            </Grid>
            </>
        );
    }
}

export default FixedSizeGrid;