import React from 'react';
import { Row, Col} from 'react-bootstrap';

import '../../../styles/styles.css';

const RowTemplate = (props) => {

    const { isScrolling, style, index } = props;

    if(!isScrolling){
    return(
        <Row className={index % 2 ? 'ListItemOdd' : 'ListItemEven'} style={style}>
            <Col>
                Column 1 : Row {index}
            </Col>
            <Col>
                Column 2 : Row {index}
            </Col>
            <Col>
                Column 3 : Row {index}
            </Col>
        </Row>
    )
    }else{
        return(
            <Row className={index % 2 ? 'ListItemOdd' : 'ListItemEven'} style={style}>
                <Col>
                    Loading.... Row {index}
                </Col>
            </Row>
        )   
    }
}

export default RowTemplate;