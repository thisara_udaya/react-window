import React, { PureComponent } from 'react';

import {
  LOADED
}from '../constants/infiniteListRowConstants';

class InfiniteListRowTemplate extends PureComponent {
    render() {
      const { index, style, data } = this.props;
      let label;
      if (data[index] === LOADED) {
        label = `Item ${index}`;
      } else {
        label = "Loading...";
      }
      return (
        <div className="ListItem" style={style}>
          {label}
        </div>
      );
    }
}

export default InfiniteListRowTemplate;