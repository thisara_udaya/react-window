const LOADING = 1;
const LOADED = 2;

export {
    LOADING,
    LOADED
}