import {
    LOADING,
    LOADED
} from '../constants/infiniteListRowConstants';

let itemStatusMap = {};

const isItemLoaded = index => !!itemStatusMap[index];

const loadMoreItems = (startIndex, stopIndex) => {
    for (let index = startIndex; index <= stopIndex; index++) {
        itemStatusMap[index] = LOADING;
    }
    //example wait time to visualize the loading
    return new Promise(resolve =>
        setTimeout(() => {
        for (let index = startIndex; index <= stopIndex; index++) {
            itemStatusMap[index] = LOADED;
        }
        resolve();
        }, 3000)
    );
};

export {
    isItemLoaded,
    loadMoreItems,
    itemStatusMap
}