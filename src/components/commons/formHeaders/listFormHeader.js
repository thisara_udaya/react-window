import React from 'react';
import { ButtonGroup, DropdownButton, Dropdown, Button, Form, Container, Row, Col } from 'react-bootstrap';

import { ALIGN_AUTO, ALIGN_START, ALIGN_SMART, ALIGN_END, ALIGN_CENTER } from '../constants/formHeaderConstants';

const FormHeader = (props) => {
    return (
        <Container className='formHeader'>
            <Row>
                <Col aria-colspan={4}>
                    <h2 className='formTitle'>{props.formHeader}</h2>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form.Control type="number" placeholder="Enter row number" onChange={props.onUpdateScrollTo}/>
                </Col>
                <Col>
                <ButtonGroup onClick={props.onUpdateAlign}>
                    <Button variant="secondary" value={ALIGN_AUTO}>Auto</Button>
                    <Button variant="secondary" value={ALIGN_SMART}>Smart</Button>
                    <Button variant="secondary" value={ALIGN_CENTER}>Center</Button>
                    <Button variant="secondary" value={ALIGN_END}>End</Button>
                    <Button variant="secondary" value={ALIGN_START}>Start</Button>
                </ButtonGroup>
                </Col>
                <Col>
                    <div style={{width:90, display:"inline"}}><Button variant="success" onClick={props.onScrollTo}>Search</Button></div>
                    <div style={{position:"relative", float:"right", width:90, display:"inline"}}>
                    <DropdownButton variant="success" id="dropdown-basic-button" title={props.layout.toUpperCase()} onSelect={props.onChangeLayout}>
                        <Dropdown.Item eventKey={props.VERTICAL}>{props.VERTICAL.toUpperCase()}</Dropdown.Item>
                        <Dropdown.Item eventKey={props.HORIZONTAL}>{props.HORIZONTAL.toUpperCase()}</Dropdown.Item>
                    </DropdownButton>
                    </div>
                </Col>
                <Col className='col-md-1-5'>
                    
                </Col>
            </Row>
        </Container>);
}

export default FormHeader;