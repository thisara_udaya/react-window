import React, { Component } from 'react';
import { VariableSizeGrid as Grid } from 'react-window';

import CellTemplate from '../commons/rowTemplates/gridCellTemplate';
import FormHeader from '../commons/formHeaders/gridFormHeader';

import { ALIGN_AUTO } from '../commons/constants/formHeaderConstants';

class VariableSizeGrid extends Component{

    gridRef = React.createRef();

    state = {
        scrollToRowNumber : 0,
        scrollToColumnNumber : 0,
        align : ALIGN_AUTO
    }

    onScrollTo = (e) => {
        this.gridRef.current.scrollToItem({
            columnIndex: this.state.scrollToColumnNumber,
            rowIndex: this.state.scrollToRowNumber,
            align: this.state.align
        });
    }

    onUpdateScrollToRow = (e) => {
        if(e){
            this.setState({
                scrollToRowNumber : e.target.value
            })
        }
    }

    onUpdateScrollToColumn = (e) => {
        if(e){
            this.setState({
                scrollToColumnNumber : e.target.value
            })
        }
    }

    getColumnWidth = (index) => {
        if(index%2 !== 1){
            return 200;
        }else{
            return 100;
        }
    }

    getRowHeight = (index) => {
        if(index%2 !== 1){
            return 70;
        }else{
            return 35;
        }
    }

    onUpdateAlign = (e) => {
        if(e){
            this.setState({
                align : e.target.value
            })
        }
    }

    render(){
        return (
            <>
            <FormHeader formHeader={'Variable Size Grid'} onUpdateScrollToRow={this.onUpdateScrollToRow} onUpdateScrollToColumn={this.onUpdateScrollToColumn} onScrollTo={this.onScrollTo} onUpdateAlign={this.onUpdateAlign}/>
            <Grid
                columnCount={1000}
                columnWidth={this.getColumnWidth}
                height={500}
                rowCount={1000}
                rowHeight={this.getRowHeight}
                width={900}
                useIsScrolling {...this.props}
                ref={this.gridRef}
            >
                { CellTemplate }
            </Grid>
            </>
        )
    }
}

export default VariableSizeGrid;