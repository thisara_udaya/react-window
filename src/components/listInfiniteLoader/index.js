import React, { Component } from 'react';
import { FixedSizeList as List } from 'react-window';
import InfiniteLoader from "react-window-infinite-loader";

import FormHeader from '../commons/formHeaders/listFormHeader';
import InfiniteListRowTemplate from '../commons/rowTemplates/infiniteListRowTemplate';

import { VERTICAL,HORIZONTAL, ALIGN_AUTO } from '../commons/constants/formHeaderConstants';

import {
    isItemLoaded,
    loadMoreItems,
    itemStatusMap
}from '../commons/utils/infiniteListRowLogic';

class ListInfiniteLoader extends Component{

    listRef = React.createRef();

    state = {
        layout : VERTICAL,
        scrollToRowNumber : 1,
        align : ALIGN_AUTO,
    }

    onChangeLayout = (e) => {
        if(e){
            this.setState({
                layout : e 
            });
        }
    }

    onScrollTo = (e) => {
        alert('not supported in this version!');
    }

    onUpdateScrollTo = (e) => {
        if(e){
            this.setState({
                scrollToRowNumber : e.target.value
            })
        }
    }

    onUpdateAlign = (e) => {
        if(e){
            this.setState({
                align : e.target.value
            })
        }
    }

    render(){
        return(
            <>
            <FormHeader formHeader={'Infinite List'}  VERTICAL={VERTICAL} HORIZONTAL={HORIZONTAL} layout={this.state.layout} onChangeLayout={this.onChangeLayout} onUpdateScrollTo={this.onUpdateScrollTo} onScrollTo={this.onScrollTo} onUpdateAlign={this.onUpdateAlign}/>
            <InfiniteLoader
                isItemLoaded={isItemLoaded}
                itemCount={10000}
                loadMoreItems={loadMoreItems}
                >
                {({ onItemsRendered, ref }) => (
                    <List
                        height={500}
                        itemCount={100}
                        itemSize={90}
                        itemData={itemStatusMap}
                        width={900}
                        layout={this.state.layout}
                        onItemsRendered={onItemsRendered}
                        ref={ref}
                    >
                        {InfiniteListRowTemplate}
                </List>
                )}
            </InfiniteLoader>
          </>
        )
    }
}

export default ListInfiniteLoader;