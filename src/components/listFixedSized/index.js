import React, { Component } from 'react';
import { FixedSizeList as List } from 'react-window';

import RowTemplate from '../commons/rowTemplates/listRowTemplate';
import FormHeader from '../commons/formHeaders/listFormHeader';

import { VERTICAL,HORIZONTAL, ALIGN_AUTO } from '../commons/constants/formHeaderConstants';

class FixedSizeList extends Component{

    listRef = React.createRef();

    state = {
        layout : VERTICAL,
        scrollToRowNumber : 1,
        align : ALIGN_AUTO
    }

    onChangeLayout = (e) => {
        if(e){
            this.setState({
                layout : e 
            });
        }
    }

    onScrollTo = (e) => {
        this.listRef.current.scrollToItem(this.state.scrollToRowNumber, this.state.align);
    }

    onUpdateScrollTo = (e) => {
        if(e){
            this.setState({
                scrollToRowNumber : e.target.value
            })
        }
    }

    onUpdateAlign = (e) => {
        if(e){
            this.setState({
                align : e.target.value
            })
        }
    }

    render(){
        return(
            <>
            <FormHeader formHeader={'Fixed Size List'}  VERTICAL={VERTICAL} HORIZONTAL={HORIZONTAL} layout={this.state.layout} onChangeLayout={this.onChangeLayout} onUpdateScrollTo={this.onUpdateScrollTo} onScrollTo={this.onScrollTo} onUpdateAlign={this.onUpdateAlign}/>
            <List
                height={500}
                itemCount={100}
                itemSize={90}
                width={900}
                layout={this.state.layout}
                ref={this.listRef}
            >
                {RowTemplate} 
          </List>
          </>
        )
    }
}

export default FixedSizeList;